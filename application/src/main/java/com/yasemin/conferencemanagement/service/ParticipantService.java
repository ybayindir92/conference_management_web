package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParticipantService {

    private final ParticipantRepository participantRepository;

    public List<Participant> getAll() {
        return participantRepository.findAll();
    }

}
