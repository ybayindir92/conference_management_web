package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;

    public List<Room> getAll() {
        return roomRepository.findAll();
    }

}
