package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.repository.ConferenceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConferenceService {

    private final ConferenceRepository conferenceRepository;

    public void save(final Conference conference) {
        conferenceRepository.save(conference);
    }

    public Conference getConference(final Long id) {
        return conferenceRepository.findById(id).orElse(null);
    }

    public List<Conference> getAll() {
        return conferenceRepository.getAllByOrderByStartDate();
    }

    public void delete(final long id) {
        conferenceRepository.deleteById(id);
    }

    public boolean isExist(final long id) {
        return conferenceRepository.existsById(id);
    }

    public List<String> dateAndRoomValidation(final Conference conference) {
        List<String> validationErrors = new ArrayList<>();

        boolean endDateAfterThanStartDate = conference.getEndDate().after(conference.getStartDate());
        if (!endDateAfterThanStartDate) {
            validationErrors.add("End date must be later than start date!");
        }

        boolean roomIsAvailable = conferenceRepository.getByRoomAndEndDateGreaterThanAndStartDateLessThan(
                conference.getRoom(), conference.getStartDate(), conference.getEndDate()).isEmpty();
        if (!roomIsAvailable) {
            validationErrors.add("The room isn't suitable for the selected date range!");
        }
        return validationErrors;
    }

    public boolean isThereAnyAvailableSeat(final Conference conference) {
        if (conference.getParticipants() != null) {
            int availableSeats = calculateAvailableSeats(conference);
            return availableSeats > 0;
        }
        return true;
    }

    private int calculateAvailableSeats(final Conference conference) {
        return conference.getRoom().getMaxSeat() - conference.getParticipants().size();
    }

}
