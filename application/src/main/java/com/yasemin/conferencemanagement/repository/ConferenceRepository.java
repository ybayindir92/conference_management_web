package com.yasemin.conferencemanagement.repository;

import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {

    List<Conference> getAllByOrderByStartDate();

    List<Conference> getByRoomAndEndDateGreaterThanAndStartDateLessThan(
            final Room room, final Date startDate, final Date endDate);

}
