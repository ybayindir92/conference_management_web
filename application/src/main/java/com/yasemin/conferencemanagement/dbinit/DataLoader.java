package com.yasemin.conferencemanagement.dbinit;

import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.ParticipantRepository;
import com.yasemin.conferencemanagement.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationRunner {

    private final RoomRepository roomRepository;
    private final ParticipantRepository participantRepository;

    @Override
    public void run(ApplicationArguments args) {
        createRoom("M/S Baltic Queen Room", "M/S Baltic Queen", 124);
        createRoom("Blue Room", "Tallinn", 50);
        createRoom("Green Room", "Stockholm", 20);
        createRoom("Red Room", "Helsinki", 5);
        createRoom("White Room", "London", 5);

        createParticipant("Yasemin Bayindir", 1992, 1, 1);
        createParticipant("Yury Repeshov", 1992, 2, 25);
        createParticipant("Anton Kasper", 1978, 5, 10);
        createParticipant("Alex White", 1987, 11, 12);
        createParticipant("Katy Jones", 1995, 12, 9);
        createParticipant("Ali Demir", 1976, 5, 14);
        createParticipant("Penny Kaev", 1985, 4, 1);
        createParticipant("Robert Cooper", 1998, 3, 15);
        createParticipant("Tobias Martin", 1987, 6, 21);
        createParticipant("Ninna Artemo", 1994, 1, 9);
        createParticipant("Havva Ozi", 1986, 8, 11);
        createParticipant("Dimitri Nikolai", 1986, 8, 11);
        createParticipant("Kadri Kai", 1985, 3, 18);
        createParticipant("Natalia Perla", 1986, 8, 19);
        createParticipant("Lily Aldrin", 1976, 8, 24);
        createParticipant("Ted Mosby", 1981, 8, 7);
    }

    private void createRoom(final String name, final String location, final int maxSeat) {
        roomRepository.save(Room.builder().name(name).location(location).maxSeat(maxSeat).build());
    }

    private void createParticipant(final String name, final int birthYear, final int birthMonth, final int birthDay) {
        participantRepository.save(
                Participant.builder().name(name).birthday(createDate(birthYear, birthMonth, birthDay)).build());
    }

    private Date createDate(int year, int month, int day) {
        return Date.valueOf(LocalDate.of(year, month, day));
    }

}
