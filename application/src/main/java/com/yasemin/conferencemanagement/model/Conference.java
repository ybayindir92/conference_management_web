package com.yasemin.conferencemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Length(max = 150)
    private String title;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "ROOM_ID")
    @NotNull
    private Room room;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONFERENCE_PARTICIPANT",
            joinColumns = {@JoinColumn(name = "CONFERENCE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "PARTICIPANT_ID")})
    private List<Participant> participants;

}
