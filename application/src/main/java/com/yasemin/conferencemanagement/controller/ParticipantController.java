package com.yasemin.conferencemanagement.controller;

import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ParticipantController {

    private final ParticipantService participantService;

    @GetMapping(value = "/participants")
    public ResponseEntity<List<Participant>> allParticipants() {
        List<Participant> participants = participantService.getAll();
        if (participants.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(participants, HttpStatus.OK);
    }

}
