package com.yasemin.conferencemanagement.controller;

import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.service.ConferenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ConferenceController {

    private final ConferenceService conferenceService;

    @PostMapping(value = "/conference", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addConference(@RequestBody @Valid Conference conference, BindingResult bindingResult) {

        List<String> validationErrors = conferenceService.dateAndRoomValidation(conference);
        if (bindingResult.hasErrors() || !validationErrors.isEmpty()) {
            validationErrors.addAll(
                    bindingResult.getAllErrors().stream().map(ObjectError::toString).collect(Collectors.toList()));
            return new ResponseEntity<>(validationErrors, HttpStatus.BAD_REQUEST);
        }
        conferenceService.save(conference);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/conferences")
    public ResponseEntity<List<Conference>> allConferences() {
        List<Conference> conferences = conferenceService.getAll();
        if (conferences.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(conferences, HttpStatus.OK);
    }

    @DeleteMapping(value = "/conference/{id}")
    public ResponseEntity deleteConference(@PathVariable("id") long id) {
        if (!conferenceService.isExist(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        conferenceService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/conference/{id}")
    public ResponseEntity<Conference> getConference(@PathVariable("id") long id) {
        if (!conferenceService.isExist(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final Conference conference = conferenceService.getConference(id);
        return new ResponseEntity<>(conference, HttpStatus.OK);
    }

    @PutMapping(value = "/conference", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateConference(@RequestBody @Valid Conference conference, BindingResult bindingResult) {
        if (conference.getId() == null || !conferenceService.isExist(conference.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<String> validationErrors = new ArrayList<>();
        if (!conferenceService.isThereAnyAvailableSeat(conference)) {
            validationErrors.add("Conference is already full");
        }
        if (bindingResult.hasErrors()) {
            validationErrors.addAll(bindingResult.getAllErrors().stream()
                    .map(ObjectError::toString).collect(Collectors.toList()));
            return new ResponseEntity<>(validationErrors, HttpStatus.BAD_REQUEST);
        }
        conferenceService.save(conference);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
