package com.yasemin.conferencemanagement.controller;

import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.repository.ParticipantRepository;
import io.restassured.response.ValidatableResponse;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;

import static io.restassured.RestAssured.get;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

public class ParticipantControllerIntegrationTest extends MvcIntegrationTest {

    @MockBean
    private ParticipantRepository participantRepository;

    @Test
    public void shouldReturnAllParticipants() {
        // given
        BDDMockito.given(participantRepository.findAll()).willReturn(Arrays.asList(
                Participant.builder().id(1L).name("Alex").birthday(Date.valueOf(LocalDate.of(2018, 1, 1))).build(),
                Participant.builder().id(2L).name("John").birthday(Date.valueOf(LocalDate.of(2018, 2, 2))).build()));

        // when
        final ValidatableResponse response = get("/api/participants").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", is(2));
        response.assertThat().body("id", hasItems(1, 2));
        response.assertThat().body("name", hasItems("Alex", "John"));
        response.assertThat().body("birthday", hasItems("2018-01-01", "2018-02-02"));
    }

    @Test
    public void shouldReturnNoContentWhenThereIsNoParticipant() {
        // given
        BDDMockito.given(participantRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/api/participants").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

}
