package com.yasemin.conferencemanagement.controller;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class MvcIntegrationTest {

    @Autowired
    Environment environment;

    @Before
    public void setUp() {
        RestAssured.port = environment.getProperty("local.server.port", Integer.class);
    }

}
