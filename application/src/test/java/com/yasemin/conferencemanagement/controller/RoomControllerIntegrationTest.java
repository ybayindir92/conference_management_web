package com.yasemin.conferencemanagement.controller;

import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.RoomRepository;
import io.restassured.response.ValidatableResponse;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

import static io.restassured.RestAssured.get;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;

public class RoomControllerIntegrationTest extends MvcIntegrationTest {

    @MockBean
    private RoomRepository roomRepository;

    @Test
    public void shouldReturnAllRooms() {
        // given
        BDDMockito.given(roomRepository.findAll()).willReturn(Arrays.asList(
                Room.builder().id(1L).name("room1").location("location1").maxSeat(10).build(),
                Room.builder().id(2L).name("room2").location("location2").maxSeat(20).build()));

        // when
        final ValidatableResponse response = get("/api/rooms").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", is(2));
        response.assertThat().body("id", hasItems(1, 2));
        response.assertThat().body("name", hasItems("room1", "room2"));
        response.assertThat().body("location", hasItems("location1", "location2"));
        response.assertThat().body("maxSeat", hasItems(10, 20));
    }

    @Test
    public void shouldReturnNoContentWhenThereIsNoRoom() {
        // given
        BDDMockito.given(roomRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/api/rooms").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

}