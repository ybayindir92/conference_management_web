package com.yasemin.conferencemanagement.controller;

import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.ConferenceRepository;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

import static io.restassured.RestAssured.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class ConferenceControllerIntegrationTest extends MvcIntegrationTest {

    @MockBean
    ConferenceRepository conferenceRepository;

    @Test
    public void shouldReturnAllConferences() {
        // given
        BDDMockito.given(conferenceRepository.getAllByOrderByStartDate())
                .willReturn(asList(new Conference(), new Conference()));

        // when
        final ValidatableResponse response = get("/api/conferences").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", is(2));
    }

    @Test
    public void shouldReturnNoContentWhenThereIsNoConference() {
        // given
        BDDMockito.given(conferenceRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/api/conferences").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnConferenceWithRoomAndParticipants() {
        // given
        BDDMockito.given(conferenceRepository.existsById(5L)).willReturn(true);
        BDDMockito.given(conferenceRepository.findById(5L)).willReturn(Optional.of(
                Conference.builder().id(5L).title("conferenceTitle")
                        .startDate(Date.valueOf(LocalDate.of(2000, 2, 2)))
                        .endDate(Date.valueOf(LocalDate.of(2000, 2, 3)))
                        .room(Room.builder().name("room1").build())
                        .participants(asList(
                                Participant.builder().name("yasemin").build(),
                                Participant.builder().name("ninna").build()))
                        .build()));

        // when
        final ValidatableResponse response = get("/api/conference/{id}", "5").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("id", equalTo(5));
        response.assertThat().body("startDate", equalTo("2000-02-02"));
        response.assertThat().body("endDate", equalTo("2000-02-03"));
        response.assertThat().body("title", equalTo("conferenceTitle"));
        response.assertThat().body("room.name", equalTo("room1"));
        response.assertThat().body("participants.name", hasItems("yasemin", "ninna"));
    }

    @Test
    public void roomShouldNotContainConferenceWhenConferenceIsRequested() {
        // given
        Room room = Room.builder().name("room1").build();
        Conference conference = Conference.builder().id(5L).title("title").build();
        conference.setRoom(room);
        room.setConferences(singletonList(conference));
        BDDMockito.given(conferenceRepository.existsById(5L)).willReturn(true);
        BDDMockito.given(conferenceRepository.findById(5L)).willReturn(Optional.of(conference));

        // when
        final ValidatableResponse response = get("/api/conference/{id}", "5").then();

        //then
        response.assertThat().body("room", not(hasKey("conferences")));
    }

    @Test
    public void participantsShouldNotContainConferencesWhenConferenceIsRequested() {
        // given
        Conference conference = Conference.builder().id(5L).title("title").build();
        Participant participant = Participant.builder().name("anton").conferences(singletonList(conference)).build();
        conference.setParticipants(singletonList(participant));
        BDDMockito.given(conferenceRepository.existsById(5L)).willReturn(true);
        BDDMockito.given(conferenceRepository.findById(5L)).willReturn(Optional.of(conference));

        // when
        final ValidatableResponse response = get("/api/conference/{id}", "5").then();

        //then
        response.assertThat().body("participants[0]", hasKey("name"));
        response.assertThat().body("participants[0]", not(hasKey("conferences")));
    }

    @Test
    public void shouldReturnNotFoundWhenConferenceIsNotExist() {
        // given
        BDDMockito.given(conferenceRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/api/conferences").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnNotFoundWhenThereIsNoConferenceToDelete() {
        // given
        BDDMockito.given(conferenceRepository.existsById(5L)).willReturn(false);

        // when
        final ValidatableResponse response = delete("/api/conference/{id}", "5").then();

        // then
        response.assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldDeleteConference() {
        // given
        BDDMockito.given(conferenceRepository.existsById(5L)).willReturn(true);

        // when
        final ValidatableResponse response = delete("/api/conference/{id}", "5").then();

        // then
        verify(conferenceRepository).deleteById(5L);
        response.assertThat().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void shouldNotSaveWhenValidationErrorOccurs() {
        // given
        Conference conference = Conference.builder()
                .startDate(Date.valueOf("2018-01-01"))
                .endDate(Date.valueOf("2018-01-02")).build();
        final RequestSpecification request = given().contentType(APPLICATION_JSON_VALUE).body(conference);

        // when
        final ValidatableResponse response = request.post("/api/conference/").then();

        // then
        response.assertThat().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void shouldSaveConference() {
        // given
        Conference conference = Conference.builder()
                .title("conference")
                .startDate(Date.valueOf("2018-01-01"))
                .endDate(Date.valueOf("2018-01-02"))
                .room(new Room()).build();
        final RequestSpecification request = given().contentType(APPLICATION_JSON_VALUE).body(conference);

        // when
        final ValidatableResponse response = request.post("/api/conference/").then();

        // then
        response.assertThat().statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void shouldNotUpdateWhenValidationErrorOccurs() {
        // given
        Conference conference = Conference.builder().id(3L).build();
        BDDMockito.given(conferenceRepository.existsById(3L)).willReturn(true);
        final RequestSpecification request = given().contentType(APPLICATION_JSON_VALUE).body(conference);

        // when
        final ValidatableResponse response = request.put("/api/conference/").then();

        // then
        response.assertThat().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void shouldReturnNotFoundWhenThereIsNoConferenceToUpdate() {
        // given
        Conference conference = Conference.builder().id(8L).build();
        BDDMockito.given(conferenceRepository.existsById(8L)).willReturn(false);
        final RequestSpecification request = given().contentType(APPLICATION_JSON_VALUE).body(conference);

        // when
        final ValidatableResponse response = request.put("/api/conference/").then();

        // then
        response.assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldUpdateConference() {
        // given
        BDDMockito.given(conferenceRepository.existsById(2L)).willReturn(true);
        Conference conference = Conference.builder()
                .id(2L)
                .title("conference")
                .startDate(Date.valueOf("2018-01-01"))
                .endDate(Date.valueOf("2018-01-02"))
                .room(new Room()).build();
        final RequestSpecification request = given().contentType(APPLICATION_JSON_VALUE).body(conference);

        // when
        final ValidatableResponse response = request.put("/api/conference/").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
    }

}
