package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.repository.ParticipantRepository;
import com.yasemin.conferencemanagement.service.ParticipantService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantsServiceUnitTest {

    @Mock
    private ParticipantRepository participantRepository;

    @InjectMocks
    private ParticipantService participantService;

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        participantService.getAll();

        // then
        verify(participantRepository).findAll();
    }

}