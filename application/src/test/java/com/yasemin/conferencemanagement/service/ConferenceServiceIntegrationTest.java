package com.yasemin.conferencemanagement.service;


import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.ConferenceRepository;
import com.yasemin.conferencemanagement.repository.RoomRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ConferenceServiceIntegrationTest {

    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Test
    public void shouldReturnValidationErrorWhenRoomIsNotAvailableAtThatTime() {
        // given
        Room room = Room.builder().name("room").location("location").maxSeat(10).build();
        roomRepository.saveAndFlush(room);
        conferenceRepository.saveAndFlush(Conference.builder().room(room)
                .title("title")
                .startDate(createDateTime(18, 0))
                .endDate(createDateTime(19, 0)).build());

        // when
        final List<String> validationErrors = conferenceService.dateAndRoomValidation(
                Conference.builder().room(room).startDate(createDateTime(18, 30)).endDate(createDateTime(19, 30)).build());

        // then
        assertThat(validationErrors).contains("The room isn't suitable for the selected date range!");
    }


    private Date createDateTime(final int hour, final int minute) {
        return Timestamp.valueOf(LocalDateTime.of(2018, 1, 1, hour, minute));
    }

}