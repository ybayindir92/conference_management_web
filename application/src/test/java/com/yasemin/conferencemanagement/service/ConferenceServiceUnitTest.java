package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.model.Conference;
import com.yasemin.conferencemanagement.model.Participant;
import com.yasemin.conferencemanagement.model.Room;
import com.yasemin.conferencemanagement.repository.ConferenceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ConferenceServiceUnitTest {

    @Mock
    private ConferenceRepository conferenceRepository;

    @InjectMocks
    private ConferenceService conferenceService;

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        conferenceService.getAll();

        // then
        verify(conferenceRepository).getAllByOrderByStartDate();
    }

    @Test
    public void shouldCallFindByIdMethodOfRepository() {
        // when
        conferenceService.getConference(5L);

        // then
        verify(conferenceRepository).findById(5L);
    }

    @Test
    public void shouldCallSaveMethodOfRepository() {
        // given
        final Conference conference = Conference.builder().build();

        // when
        conferenceService.save(conference);

        // then
        verify(conferenceRepository).save(conference);
    }

    @Test
    public void shouldCallDeleteMethodOfRepository() {
        // given
        final Long id = 5L;

        // when
        conferenceService.delete(id);

        // then
        verify(conferenceRepository).deleteById(id);
    }

    @Test
    public void shouldCallIsExistMethodOfRepository() {
        // given
        final Long id = 5L;

        // when
        conferenceService.isExist(id);

        // then
        verify(conferenceRepository).existsById(id);
    }

    @Test
    public void shouldReturnValidationErrorWhenRoomStartDateAndEndDateSame() {
        // given
        final Conference conference = Conference.builder()
                .startDate(Timestamp.valueOf(LocalDateTime.of(2018, 1, 1, 18, 30)))
                .endDate(Timestamp.valueOf(LocalDateTime.of(2018, 1, 1, 18, 30))).build();

        // when
        final List<String> validationErrors = conferenceService.dateAndRoomValidation(conference);

        // then
        assertThat(validationErrors).contains("End date must be later than start date!");
    }

    @Test
    public void shouldReturnFalseWhenRoomCapacityFull() {
        // given
        final Conference conference = Conference.builder()
                .room(Room.builder().maxSeat(3).build())
                .participants(asList(new Participant(), new Participant(), new Participant())).build();

        // when
        final boolean actual = conferenceService.isThereAnyAvailableSeat(conference);

        // then
        assertThat(actual).isFalse();
    }

}