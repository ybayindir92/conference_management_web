package com.yasemin.conferencemanagement.service;

import com.yasemin.conferencemanagement.repository.RoomRepository;
import com.yasemin.conferencemanagement.service.RoomService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RoomServiceUnitTest {

    @Mock
    private RoomRepository roomRepository;

    @InjectMocks
    private RoomService roomService;

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        roomService.getAll();

        // then
        verify(roomRepository).findAll();
    }

}