# Conference Management System

Conference Management System is a web application to create conferences and manage them.   
Default rooms can be chosen for a conference and participants can be added depend on the room capacity.   
The system also detects room availability.   



##### To start with locally...
'gradlew bootFullApplication' command is enough for running the application.  
This task installs the node.js, builds frontend and backend.  
First run can take a bit time because of node.js download and npm install.


##### What can be improved?
Currently, all participants and conference rooms are added in the backend.
They should have their own insert/update/delete pages.   
UI Tests are missing :(


##### Technologies
* Spring Boot
* Spring Data
* Spring Mvc
* Angular 6
* Bootstrap
* Restful Api
* REST-assured
* H2
* Gradle