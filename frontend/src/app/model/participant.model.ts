import {Conference} from "./conference.model";

export class Participant {

  id: number;
  name: string;
  birthday: string;
  conferences: Conference[];

}
