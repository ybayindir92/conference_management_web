import {Room} from "./room.model";
import {Participant} from "./participant.model";

export class Conference {

  id: number;
  title: string;
  startDate: Date;
  endDate: Date;
  room: Room;
  participants: Participant[];
  availableSeats: number;

  constructor() {
    this.id = null;
    this.title = "";
    this.room = null;
    this.startDate = new Date();
    this.endDate = new Date();
    this.participants = null;
    this.availableSeats = null;
  }

}
