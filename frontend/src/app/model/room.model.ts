import {Conference} from "./conference.model";

export class Room {

  id: number;
  name: string;
  location: string;
  maxSeat: number;
  conferences: Conference[];

}
