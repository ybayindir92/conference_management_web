import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';
import {ConferenceComponent} from './conference/conference.component';
import {RoomService} from "./service/room.service";
import {ConferenceService} from "./service/conference.service";
import {ConferenceListComponent} from './conference-list/conference-list.component';
import {CalendarModule} from 'primeng/calendar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './/app-routing.module';
import {ConferenceParticipantComponent} from './conference-participant/conference-participant.component';
import {ParticipantService} from "./service/participant.service";

@NgModule({
  declarations: [
    AppComponent,
    ConferenceComponent,
    ConferenceListComponent,
    ConferenceParticipantComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CalendarModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [RoomService, ConferenceService, ParticipantService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
