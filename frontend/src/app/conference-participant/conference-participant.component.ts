import {Component, OnInit} from '@angular/core';
import {Conference} from "../model/conference.model";
import {ConferenceService} from "../service/conference.service";
import {Participant} from "../model/participant.model";
import {ParticipantService} from "../service/participant.service";
import sweetAlert from "sweetalert2"

@Component({
  selector: 'app-conference-participant',
  templateUrl: './conference-participant.component.html',
  styleUrls: ['./conference-participant.component.css']
})
export class ConferenceParticipantComponent implements OnInit {

  conferenceList: Conference[];
  potentialParticipants: Participant[];
  selectedConference: Conference;
  selectedParticipant: Participant;
  participantHasError = true;

  constructor(private conferenceService: ConferenceService, private participantService: ParticipantService) {
  }

  ngOnInit() {
    this.getAllConferences();
    this.getAllParticipants();
    this.selectedParticipant = null;
  }

  getAllConferences() {
    this.conferenceService.getAll()
      .subscribe(conferenceList => {
          this.conferenceList = conferenceList;
        }
      );
  }

  getAllParticipants() {
    this.participantService.getAll()
      .subscribe(participantList => {
        this.potentialParticipants = participantList;
      })
  }

  selectConference() {
    this.potentialParticipants = this.filterParticipantList();
    this.selectedConference.availableSeats = this.conferenceService.calculateAvailableSeats(this.selectedConference);
  }

  addParticipant(participant) {
    if (this.selectedConference.availableSeats == 0) {
      sweetAlert({
        title: 'Conference is already full',
        type: 'warning'
      });
    } else if (this.selectedConference.participants.some(p => p.id === participant.id)) {
      sweetAlert({
        title: 'Participant is already attended',
        type: 'warning'
      });
    } else {
      this.conferenceService.update(this.selectedConference)
        .subscribe(
          () => {
            this.selectedConference.participants.push(participant);
            this.selectedConference.availableSeats--;
            this.updateConferenceParticipant();
          },
          error => {
            let message: string = error.error.join('/n');
            sweetAlert({
              title: 'Oops...',
              text: message,
              type: 'error'
            })
          }
        );
    }
    this.selectedParticipant = null;
  }

  deleteParticipant(participant) {
    sweetAlert({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        const index = this.selectedConference.participants.findIndex(
          participantInConference => participantInConference.id === participant.id);
        this.selectedConference.participants.splice(index, 1);
        this.selectedConference.availableSeats++;
        this.potentialParticipants.push(participant);
        this.updateConferenceParticipant();
      }
    });
  }

  updateConferenceParticipant() {
    this.conferenceService.update(this.selectedConference)
      .subscribe(() => {
        this.potentialParticipants = this.filterParticipantList();
      });
  }

  filterParticipantList() {
    for (let participant of this.selectedConference.participants) {
      this.potentialParticipants = this.potentialParticipants.filter(p => p.id !== participant.id);
    }
    return this.potentialParticipants;
  }

  validateParticipant(participant) {
    this.participantHasError = Boolean(!participant || !participant.name);
  }

}
