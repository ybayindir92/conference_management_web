import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConferenceParticipantComponent} from './conference-participant.component';

describe('ConferenceParticipantComponent', () => {
  let component: ConferenceParticipantComponent;
  let fixture: ComponentFixture<ConferenceParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConferenceParticipantComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
