import {Component, Input, OnInit} from '@angular/core';
import {ConferenceService} from "../service/conference.service";
import sweetAlert from 'sweetalert2'
import {Conference} from "../model/conference.model";

@Component({
  selector: 'app-conference-list',
  templateUrl: './conference-list.component.html',
  styleUrls: ['./conference-list.component.css']
})
export class ConferenceListComponent implements OnInit {

  @Input() conferences: Conference[];

  constructor(private conferenceService: ConferenceService) {
  }

  ngOnInit() {
  }

  cancel(id) {
    sweetAlert({
      title: 'Are you sure?',
      type: 'question',
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        this.conferenceService.delete(id)
          .subscribe(() => {
            this.conferences = this.conferences.filter(conference => conference.id !== id);
          });
      }
    });
  }

}
