import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConferenceComponent} from "./conference/conference.component";
import {ConferenceParticipantComponent} from "./conference-participant/conference-participant.component";

const routes: Routes = [
  {path: '', redirectTo: '/conference', pathMatch: 'full'},
  {path: 'conference', component: ConferenceComponent},
  {path: 'conference-participant', component: ConferenceParticipantComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule {
}
