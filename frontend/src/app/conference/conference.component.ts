import {Component, OnInit} from '@angular/core';
import {Room} from "../model/room.model";
import {Conference} from "../model/conference.model";
import {RoomService} from "../service/room.service";
import {ConferenceService} from "../service/conference.service";
import sweetAlert from 'sweetalert2'

@Component({
  selector: 'app-conference',
  templateUrl: './conference.component.html',
  styleUrls: ['./conference.component.css']
})
export class ConferenceComponent implements OnInit {

  roomList: Room[];
  conference: Conference;
  conferenceList: Conference[];
  roomHasError = true;

  constructor(private roomService: RoomService, private conferenceService: ConferenceService) {
  }

  ngOnInit() {
    this.getAllRooms();
    this.getAllConferences();
    this.conference = new Conference();
  }

  saveConference(conferenceForm) {
    this.conferenceService.save(this.conference)
      .subscribe(
        () => {
          sweetAlert({
            title: 'Conference has been created',
            type: 'success',
            timer: 1500,
            showConfirmButton: false
          });
          conferenceForm.reset();
          this.ngOnInit();
        },
        error => {
          let message: string = error.error.join('/n');
          sweetAlert({
            title: 'Oops...',
            text: message,
            type: 'error'
          })
        }
      );
  }

  getAllConferences() {
    this.conferenceService.getAll()
      .subscribe(data => {
        if (data) {
          this.conferenceList = data;
          this.conferenceList.forEach(
            conference => conference.availableSeats = this.conferenceService.calculateAvailableSeats(conference));
        }
      });
  }

  getAllRooms() {
    this.roomService.getAll()
      .subscribe(roomList => {
        this.roomList = roomList;
      });
  }

  validateRoom(room) {
    this.roomHasError = Boolean(!room || !room.name);
  }

}
