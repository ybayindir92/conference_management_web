import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Participant} from "../model/participant.model";

@Injectable()
export class ParticipantService {

  constructor(private http: HttpClient) {
  }

  private url = '/api/';

  public getAll() {
    return this.http.get<Participant[]>(this.url + 'participants');
  }

}
