import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Conference} from "../model/conference.model";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable()
export class ConferenceService {

  constructor(private http: HttpClient) {
  }

  private url = '/api/';

  save(conference) {
    return this.http.post(this.url + 'conference', conference)
      .pipe(catchError(ConferenceService.errorHandler))
  }

  delete(id) {
    return this.http.delete(this.url + 'conference/' + id);
  }

  getAll() {
    return this.http.get<Conference[]>(this.url + 'conferences');
  }

  update(conference) {
    return this.http.put(this.url + 'conference', conference)
      .pipe(catchError(ConferenceService.errorHandler))
  }

  public calculateAvailableSeats(conference: Conference): number {
    return conference.room.maxSeat - conference.participants.length;
  }

  static errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }

}
