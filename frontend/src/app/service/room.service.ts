import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Room} from "../model/room.model";

@Injectable()
export class RoomService {

  constructor(private http: HttpClient) {
  }

  private url = '/api/';

  public getAll() {
    return this.http.get<Room[]>(this.url + 'rooms');
  }
}
